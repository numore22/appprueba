package com.example.demo.models.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="item")
public class Item implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="item_Id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long itemId;
	
	@Column(name="item_Name")
	private String itemName;
	
	@Column(name="item_Entered_By_User")
	private String itemEnteredByUser;
	
	@Column(name="item_Entered_Date")
	@Temporal(TemporalType.DATE)
	private Date itemEnteredDate;
	
	@Column(name="item_Buying_Price")
	private Double itemBuyingPrice;
	
	@Column(name="item_Selling_Price")
	private Double itemSellingPrice;
	
	@Temporal(TemporalType.DATE)
	@Column(name="item_Last_Modified_Date")
	private Date itemLastModifiedDate;
	
	@Column(name="item_Last_Modified_By_User")
	private String itemLastModifiedByUser;
	
	@Column(name="item_Status")
	private String itemStatus;
	

	public Double getItemSellingPrice() {
		return itemSellingPrice;
	}

	public void setItemSellingPrice(Double itemSellingPrice) {
		this.itemSellingPrice = itemSellingPrice;
	}

	public Date getItemLastModifiedDate() {
		return itemLastModifiedDate;
	}

	public void setItemLastModifiedDate(Date itemLastModifiedDate) {
		this.itemLastModifiedDate = itemLastModifiedDate;
	}

	public String getItemLastModifiedByUser() {
		return itemLastModifiedByUser;
	}

	public void setItemLastModifiedByUser(String itemLastModifiedByUser) {
		this.itemLastModifiedByUser = itemLastModifiedByUser;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemEnteredByUser() {
		return itemEnteredByUser;
	}

	public void setItemEnteredByUser(String itemEnteredByUser) {
		this.itemEnteredByUser = itemEnteredByUser;
	}

	public Date getItemEnteredDate() {
		return itemEnteredDate;
	}

	public void setItemEnteredDate(Date itemEnteredDate) {
		this.itemEnteredDate = itemEnteredDate;
	}

	public Double getItemBuyingPrice() {
		return itemBuyingPrice;
	}

	public void setItemBuyingPrice(Double itemBuyingPrice) {
		this.itemBuyingPrice = itemBuyingPrice;
	}
	
	
	
}
