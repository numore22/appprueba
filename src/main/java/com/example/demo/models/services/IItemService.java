package com.example.demo.models.services;

import java.util.List;

import com.example.demo.models.entities.Item;

public interface IItemService {
	
	public List<Item> findAll();
	
	public Item findById(Long id);
	
	public Item save(Item item);

	public void delete(Long id);

}
