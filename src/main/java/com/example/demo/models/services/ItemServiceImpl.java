package com.example.demo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.dao.IItemDao;
import com.example.demo.models.entities.Item;

@Service
public class ItemServiceImpl implements IItemService{
	
	@Autowired
	private IItemDao itemDao;

	@Override
	public List<Item> findAll() {
		return (List<Item>) itemDao.findAll();
	}

	@Override
	public Item findById(Long id) {
		return itemDao.findById(id).orElse(null);
	}

	@Override
	public Item save(Item item) {
		return itemDao.save(item);
	}

	@Override
	public void delete(Long id) {
		itemDao.deleteById(id);	
	}

}
