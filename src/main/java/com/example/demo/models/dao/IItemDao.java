package com.example.demo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.models.entities.Item;

public interface IItemDao extends CrudRepository<Item, Long>{

}
