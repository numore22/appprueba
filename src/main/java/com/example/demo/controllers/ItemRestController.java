package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import com.example.demo.models.entities.Item;
import com.example.demo.models.services.IItemService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/app")
public class ItemRestController {

	@Autowired
	private IItemService itemService;
	
	@GetMapping("/item/all")
	public List<Item> index(){
		return itemService.findAll();
	}
	
	@SuppressWarnings("null")
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/item")
	public List<Item> show(@RequestParam  String status, @RequestParam  String name ) {
		List<Item> lista=itemService.findAll();
		List<Item> listaReturn=new ArrayList();;
		if (lista!=null) {
		for (Item item : lista) {
			if (item.getItemName().equalsIgnoreCase(name)&&item.getItemStatus().equals(status)) {
				listaReturn.add(item);
				}
			}
		}
		return listaReturn;
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/item/{id}")
	public Item show(@PathVariable Long id) {
		if (itemService.findById(id) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
		return itemService.findById(id);
	}

	@PostMapping("/item")
	@ResponseStatus(HttpStatus.CREATED)
	public Item create(@RequestBody Item item) {
		if (itemService.findById(item.getItemId()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
		item.setItemLastModifiedDate(new Date());
		item.setItemEnteredDate(new Date());
		return itemService.save(item);	
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PutMapping("/item/{id}")
	public Item update(@RequestBody Item obj, @PathVariable Long id) {
		Item item= itemService.findById(id);
		item.setItemEnteredByUser(obj.getItemEnteredByUser());
		item.setItemEnteredDate(new Date());
		item.setItemBuyingPrice(obj.getItemBuyingPrice());
		item.setItemSellingPrice(obj.getItemSellingPrice());
		item.setItemLastModifiedDate(new Date());
		item.setItemLastModifiedByUser(obj.getItemLastModifiedByUser());
		item.setItemStatus(obj.getItemStatus());
		
		return itemService.save(item);
	}
	
	@DeleteMapping("/item/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		itemService.delete(id);
	}
}
