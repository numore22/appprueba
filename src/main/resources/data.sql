DROP TABLE IF EXISTS item;

CREATE TABLE item (
  item_Id INT AUTO_INCREMENT  PRIMARY KEY,
  item_Name VARCHAR(250) NOT NULL,
  item_Entered_By_User VARCHAR(250) NOT NULL,
  item_Entered_Date DATETIME NOT NULL,
  item_Buying_Price DOUBLE NOT NULL,
  item_Selling_Price DOUBLE NOT NULL,
  item_Last_Modified_Date DATETIME NOT NULL,
  item_Last_Modified_By_User VARCHAR(250) NOT NULL,
  item_Status VARCHAR(250) NOT NULL
);


--INSERT INTO "PUBLIC"."ITEM" (
--ITEM_ID,ITEM_BUYING_PRICE,ITEM_ENTERED_BY_USER,ITEM_ENTERED_DATE,ITEM_LAST_MODIFIED_BY_USER,ITEM_LAST_MODIFIED_DATE,ITEM_NAME,ITEM_SELLING_PRICE,ITEM_STATUS)
--VALUES('1',10,'Ale10','2022-03-23','Julu200','2022-03-23','Cargador',10.0,'Available');